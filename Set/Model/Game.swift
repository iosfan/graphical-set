//
//  Set.swift
//  Set
//
//  Created by paul on 15/01/2020.
//  Copyright © 2020 Stanford University. All rights reserved.
//

import Foundation

struct Game {
    private let cardCellsOnTable: Int

    private(set) var deck = [Card]()
    
    private(set) var cardsOnTable = [Card]()
    
    private(set) var selectedCards = [Card]()
    
    private(set) var score = 0
    
    var isTableFull: Bool {
        return cardsOnTable.count == cardCellsOnTable
    }

    var isSet: Bool {
        return Card.isSet(cards: selectedCards)
    }
    
    mutating func selectCard(with positionOnTable: Int) {
        guard positionOnTable < cardsOnTable.count else {
            return
        }

        if selectedCards.count < 3 {
            selectOrRemoveSelection(for: cardsOnTable[positionOnTable])
        } else {
            nextSelectAfter3CardsSelection(for: cardsOnTable[positionOnTable])
        }
    }
    
    mutating func deal3Cards() {
        updateScore()
        dealCards()
        selectedCards.removeAll()
    }
    
    mutating func shuffle() {
        cardsOnTable.shuffle()
    }
    
    private mutating func updateScore() {
        if isSet {
            score += 3
        } else if selectedCards.count == 3 {
            score -= 5
        }
    }
    
    private mutating func selectOrRemoveSelection(for card: Card) {
        if selectedCards.contains(card) {
            selectedCards.removeAll { $0 == card }
        } else {
            selectedCards.append(card)
        }
    }
    
    private mutating func nextSelectAfter3CardsSelection(for card: Card) {
        updateScore()

        if isSet {
            dealCards()
            if deck.isEmpty {
                cardsOnTable.removeAll { selectedCards.contains($0) }
            }
        }

        if selectedCards.contains(card) {
            selectedCards.removeAll()
        } else {
            selectedCards.removeAll()
            selectOrRemoveSelection(for: card)
        }
    }
    
    private mutating func dealCards() {
        var positionsOnTable: [Int?] = selectedCards.map { card in cardsOnTable.firstIndex(of: card)! }
        if !isSet {
            positionsOnTable = Array.init(repeating: nil, count: 3)
        }
        
        positionsOnTable.forEach { putCardOnTheTable(on: $0) }
    }
    
    private mutating func putCardOnTheTable(on position: Int?) {
        guard let lastCardFromDeck = deck.popLast() else {
            return
        }

        guard position == nil else {
            cardsOnTable[position!] = lastCardFromDeck
            return
        }

        if !isTableFull {
            cardsOnTable.append(lastCardFromDeck)
        }
    }

    init(startWithCards: Int, cardCellsOnTable: Int) {
        self.cardCellsOnTable = cardCellsOnTable
        
        for shape in Shape.allCases {
            for color in Color.allCases {
                for shading in Shading.allCases {
                    for quantity in 1...3 {
                        let card = Card(quantity: quantity, shape: shape, shading: shading, color: color)
                        deck += [card]
                    }
                }
            }
        }
        
        deck.shuffle()
        cardsOnTable += deck[deck.count-startWithCards..<deck.count]
        deck.removeSubrange(deck.count-startWithCards..<deck.count)
    }
}

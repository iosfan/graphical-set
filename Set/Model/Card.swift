//
//  Card.swift
//  Set
//
//  Created by paul on 15/01/2020.
//  Copyright © 2020 Stanford University. All rights reserved.
//

import Foundation

struct Card {
    let quantity: Int
    let shape: Shape
    let shading: Shading
    let color: Color
    
    static func isSet(cards: [Card]) -> Bool {
        guard cards.count == 3 else {
            return false
        }

        return Self.isSet(first: cards[0], second: cards[1], third: cards[2])
    }
    
    static func isSet(first: Card, second: Card, third: Card) -> Bool {
        let quantities = [first.quantity, second.quantity, third.quantity]
        let shapes = [first.shape, second.shape, third.shape]
        let shadings = [first.shading, second.shading, third.shading]
        let colors = [first.color, second.color, third.color]

        return !quantities.hasTwoOf && !shapes.hasTwoOf && !shadings.hasTwoOf && !colors.hasTwoOf
    }
}

extension Card: Hashable {
    static func == (lhs: Card, rhs: Card) -> Bool {
        return lhs.quantity == rhs.quantity
            && lhs.shape == rhs.shape
            && lhs.shading == rhs.shading
            && lhs.color == rhs.color
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(quantity)
        hasher.combine(shape)
        hasher.combine(shading)
        hasher.combine(color)
    }
}

extension Array where Element: Equatable {
    var hasTwoOf: Bool {
        for item in self {
            let entries = self.filter { element in item == element }
            if entries.count == 2 {
                return true
            }
        }
        
        return false
    }
}

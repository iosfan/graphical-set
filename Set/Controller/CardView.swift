//
//  CardView.swift
//  Set
//
//  Created by paul on 27/01/2020.
//  Copyright © 2020 Stanford University. All rights reserved.
//

import UIKit

class CardView: UIView {
    
    var color = 0
    var quantity = 0
    var shading = 0
    var shape = 0
    
    var isSelected = false { didSet { setNeedsDisplay() }}
    
    override func draw(_ rect: CGRect) {
        drawCard()
        drawSymbolsOnCard()
    }
    
    private func drawCard() {
        let roundedRect = UIBezierPath(roundedRect: bounds.insetBy(dx: spacingInParent, dy: spacingInParent), cornerRadius: cornerRadius)
        roundedRect.addClip()
        
        //background
        UIColor.white.setFill()
        roundedRect.fill()
        
        //selection
        isSelected ? UIColor.red.setStroke() : UIColor.black.setStroke()
        roundedRect.lineWidth = spacingInParent
        roundedRect.stroke()
    }

    private func drawSymbolsOnCard() {
        let rectHeight = CGFloat(bounds.size.height / 3)
        let origin = CGPoint(x: bounds.minX, y: bounds.midY - rectHeight / 2)
        let rect = CGRect(origin: origin, size: CGSize(width: bounds.size.width, height: rectHeight))
        
        switch quantity {
        case 1:
            drawShape(in: rect)
        case 2:
            let rect1 = rect.offsetBy(dx: 0, dy: -(rect.height / 2))
            drawShape(in: rect1)
            let rect2 = rect.offsetBy(dx: 0, dy: rect.height / 2)
            drawShape(in: rect2)
        case 3:
            drawShape(in: rect)
            let rect1 = rect.offsetBy(dx: 0, dy: -rect.height)
            drawShape(in: rect1)
            let rect2 = rect.offsetBy(dx: 0, dy: rect.height)
            drawShape(in: rect2)
        default:
            break
        }
    }
    
    private func drawShape(in rect: CGRect) {
        if let shape = drawSimpleShape(in: rect) {
            shape.lineWidth = lineWidth
            getColor().setStroke()
            shape.stroke()
            addShading(for: shape)
        }
    }
    
    private func drawSimpleShape(in rect: CGRect) -> UIBezierPath? {
        switch shape {
        case 1:
            return drawOval(rect)
        case 2:
            return drawDiamond(rect)
        case 3:
            return drawSquiggle(rect)
        default:
            return nil
        }
    }
    
    private func drawOval(_ rect: CGRect) -> UIBezierPath {
        let origin = CGPoint(x: rect.minX + rect.width / 4, y: rect.minY + rect.height / 4)
        let size = CGSize(width: rect.width / 2, height: rect.height / 2)
        let roundedRect = UIBezierPath(roundedRect: CGRect(origin: origin, size: size), cornerRadius: 100) // TODOL magic number here
        
        return roundedRect
    }

    private func drawDiamond(_ rect: CGRect) -> UIBezierPath {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: rect.minX + rect.width / 4, y: rect.midY))
        path.addLine(to: CGPoint(x: rect.midX, y: rect.midY - rect.height / 4))
        path.addLine(to: CGPoint(x: rect.maxX - rect.width / 4, y: rect.midY))
        path.addLine(to: CGPoint(x: rect.midX, y: rect.midY + rect.height / 4))
        path.close()
        
        return path
    }
    
    private func drawSquiggle(_ drawingArea: CGRect) -> UIBezierPath {
        let rect = CGRect(origin: CGPoint(x: drawingArea.minX + drawingArea.width / 4, y: drawingArea.minY + drawingArea.height / 4), size: CGSize(width: drawingArea.width / 2, height: drawingArea.height / 2))
        
        let upperSquiggle = UIBezierPath()
        let sqdx = rect.width * 0.1
        let sqdy = rect.height * 0.2
        upperSquiggle.move(to: CGPoint(x: rect.minX, y: rect.midY))
        upperSquiggle.addCurve(
            to: CGPoint(x: rect.minX + rect.width * 1/2, y: rect.minY + rect.height / 8),
            controlPoint1: CGPoint(x: rect.minX, y: rect.minY),
            controlPoint2: CGPoint(x: rect.minX + rect.width * 1/2 - sqdx, y: rect.minY + rect.height / 8 - sqdy))
        upperSquiggle.addCurve(
            to: CGPoint(x: rect.minX + rect.width * 4/5, y: rect.minY + rect.height / 8),
            controlPoint1: CGPoint(x: rect.minX + rect.width * 1/2 + sqdx, y: rect.minY + rect.height / 8 + sqdy),
            controlPoint2: CGPoint(x: rect.minX + rect.width * 4/5 - sqdx, y: rect.minY + rect.height / 8 + sqdy))

        upperSquiggle.addCurve(
            to: CGPoint(x: rect.minX + rect.width, y: rect.minY + rect.height / 2),
            controlPoint1: CGPoint(x: rect.minX + rect.width * 4/5 + sqdx, y: rect.minY + rect.height / 8 - sqdy ),
            controlPoint2: CGPoint(x: rect.minX + rect.width, y: rect.minY))

        let lowerSquiggle = UIBezierPath(cgPath: upperSquiggle.cgPath)
        lowerSquiggle.apply(CGAffineTransform.identity.rotated(by: CGFloat.pi))
        lowerSquiggle.apply(CGAffineTransform.identity.translatedBy(x: bounds.width, y: bounds.height))

        upperSquiggle.move(to: CGPoint(x: rect.minX, y: rect.midY))
        upperSquiggle.append(lowerSquiggle)
        
        return upperSquiggle
    }
    
    private func addShading(for path: UIBezierPath) {
        if let context = UIGraphicsGetCurrentContext() {
            context.saveGState()
            path.addClip()

            if shading == 1 {
                let stripePath = UIBezierPath()
                for x in stride(from: bounds.minX, to: bounds.maxX, by: stripeIndent) {
                    stripePath.move(to: CGPoint(x: x, y: bounds.minY))
                    stripePath.addLine(to: CGPoint(x: x, y: bounds.maxY))
                    stripePath.lineWidth = lineWidth
                }
                getColor().setStroke()
                stripePath.stroke()
            } else if shading == 2 {
                getColor().setFill()
                path.fill()
            }

            context.restoreGState()
        }
    }
}

extension CardView {
    private struct SizeRatio {
        static let cornerRadiusToBoundsHeight: CGFloat = 0.06
        static let cornerOffsetToCornerRadius: CGFloat = 0.33
        static let spacingToBoundsHeight: CGFloat = 0.035
        static let spacingToWidth: CGFloat = 0.5
        static let lineWidthToBounds: CGFloat = 0.01
    }
    private var cornerRadius: CGFloat {
        return bounds.size.height * SizeRatio.cornerRadiusToBoundsHeight
    }
    private var cornerOffset: CGFloat {
        return cornerRadius * SizeRatio.cornerOffsetToCornerRadius
    }
    
    private var spacingInParent: CGFloat {
        return bounds.size.height * SizeRatio.spacingToBoundsHeight
    }
    
    private var stripeIndent: CGFloat {
        return bounds.size.width / 18
    }
    
    private var lineWidth: CGFloat {
        return bounds.size.width * SizeRatio.lineWidthToBounds
    }
    
    private func getColor() -> UIColor {
        switch color {
        case 1:
            return UIColor.red
        case 2:
            return UIColor.green
        case 3:
            return UIColor.purple
        default:
            return UIColor.white
        }
    }
}

//
//  BoardView.swift
//  Set
//
//  Created by paul on 27/01/2020.
//  Copyright © 2020 Stanford University. All rights reserved.
//

import UIKit

@IBDesignable
class BoardView: UIView {
    var cardViews = [CardView]()

    private func setupView() {
        var grid = Grid(layout: .aspectRatio(0.7), frame: bounds)
        grid.cellCount = cardViews.count
        
        for (cellNumber, cardView) in cardViews.enumerated() {
            if let rect = grid[cellNumber] {
                cardView.frame = rect
                cardView.isOpaque = false
                cardView.backgroundColor = UIColor.clear

                addSubview(cardView)
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        cardViews.forEach { $0.removeFromSuperview() }
        setupView()
    }
}

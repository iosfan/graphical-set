//
//  ViewController.swift
//  Set
//
//  Created by paul on 09/01/2020.
//  Copyright © 2020 Stanford University. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    private lazy var game = Game(startWithCards: Constants.initialCardsNumber, cardCellsOnTable: Constants.positionsOnTableForCards)
    
    private var cardViews = [CardView]()

    @IBOutlet weak var deckLabel: UILabel!

    @IBOutlet weak var setLabel: UILabel!

    @IBOutlet weak var scoreLabel: UILabel!

    @IBOutlet weak var newGameButton: UIButton!

    @IBOutlet weak var dealButton: UIButton!
    
    @IBOutlet weak var board: BoardView! {
        didSet {
            let rotation = UIRotationGestureRecognizer(target: self, action: #selector(shuffle))
            board.addGestureRecognizer(rotation)
        }
    }
    
    @IBAction func newGame(_ sender: UIButton) {
        game = Game(startWithCards: Constants.initialCardsNumber, cardCellsOnTable: Constants.positionsOnTableForCards)
 
        updateViewFromModel()
    }

    @IBAction func selectCard(_ sender: UITapGestureRecognizer) {
        guard let cardView = sender.view as? CardView else { return }
        
        if sender.state == .ended {
            if let index = cardViews.firstIndex(of: cardView) {
                game.selectCard(with: index)
            }
        }
        
        updateViewFromModel()
    }

    @IBAction func swipeDown(_ sender: UISwipeGestureRecognizer) {
        if sender.state == .ended, sender.direction == .down {
            deal3()
        }
    }

    @IBAction func deal3Cards(_ sender: UIButton) {
        deal3()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        newGameButton.layer.cornerRadius = 16
        newGameButton.layer.borderColor = #colorLiteral(red: 0.6630887985, green: 0.9985001683, blue: 0.09665273875, alpha: 1)
        newGameButton.layer.borderWidth = CGFloat(3)
        dealButton.layer.cornerRadius = 16
        dealButton.layer.borderColor = #colorLiteral(red: 0.6630887985, green: 0.9985001683, blue: 0.09665273875, alpha: 1)
        dealButton.layer.borderWidth = CGFloat(3)

        updateViewFromModel()
    }
    
    @objc private func shuffle(_ sender: UIRotationGestureRecognizer) {
        switch sender.state {
        case .began, .changed, .ended:
            game.shuffle()
        default:
            break
        }
        
        updateViewFromModel()
    }

    private func deal3() {
        game.deal3Cards()
        
        updateViewFromModel()
    }

    private func updateViewFromModel() {
        cardViews.forEach { $0.removeFromSuperview() }
        cardViews.removeAll()
        for card in game.cardsOnTable {
            let cardView = CardView()
            cardView.color = Constants.colors[card.color]!
            cardView.shading = Constants.shadings[card.shading]!
            cardView.shape = Constants.shapes[card.shape]!
            cardView.quantity = card.quantity
            
            let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(selectCard))
            cardView.addGestureRecognizer(gestureRecognizer)

            cardViews.append(cardView)
        }
        board.cardViews = cardViews
        
        deckLabel.text = "Deck: \(game.deck.count)"
        scoreLabel.text = "Score: \(game.score)"
        setLabel.text = game.isSet ? "Set!" : "Not Set!"
        
        for (cardIndex, card) in game.cardsOnTable.enumerated() {
            cardViews[cardIndex].isSelected = game.selectedCards.contains(card)
        }
        
        dealButton.isEnabled = game.deck.count > 0 && (!game.isTableFull || game.isSet)
    }
}

extension ViewController {
    private struct Constants {
        static let initialCardsNumber = 12
        static let positionsOnTableForCards = 81
        static let colors: [Color:Int] = [.red:1, .green:2, .purple:3]
        static let shapes: [Shape:Int] = [.oval:1, .diamond:2, .squiggle:3]
        static let shadings: [Shading:Int] = [.striped:1, .solid:2, .open:3]
    }
}
